SL3D CREATION - UNITY ASSET
Thank you for downloading!

Medecine Accessories Pack

A set of 10 models of realistic medecine consumable for your games.

Tape : 51 polys
Scissors : 94 polys
Scalpel : 34 polys
Oxygen mask : 188 polys
Oxygen Bottle : 297 polys
Med Saw : 259 polys
Gauze Pads : 132 polys
Cable : 370 polys
BloodBag : 126 polys
Bandage : 68 polys


Comes with 2048x2048 PBR quality texture maps: 

- Diffuse 
- Normal Map
- Ambiant Occlusion
- Specular

Any questions or a request? 
Feel free to contact me if you want informations about an asset 
or if you need particular 3d models for your games !

http://sl3dcreation.wordpress.com