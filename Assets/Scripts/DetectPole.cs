﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPole : MonoBehaviour {

	// Use this for initialization
	public GameObject eventmanager;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name == "Pole" && !eventmanager.GetComponent<Events>().bagDone) {
			transform.position = new Vector3(-3.3f, 2, 2.7f);
			GetComponent<Rigidbody>().isKinematic = true;
			eventmanager.GetComponent<Events>().bagDone = true;

		}

		}
	}
