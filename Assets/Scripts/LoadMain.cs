﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadMain : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(LoadScene());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator LoadScene() {
		yield return new WaitForSeconds(6);
		SceneManager.LoadScene("Main");
	}
}
