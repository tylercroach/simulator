﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectTrash : MonoBehaviour {
	public GameObject eventmanager;
	public bool isTrashed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name == "Trash" && !isTrashed) {
			eventmanager.GetComponent<Events>().mugsTrashed++;
			isTrashed = true;

		}
	}
}
