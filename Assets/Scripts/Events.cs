﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Events : MonoBehaviour {
	public AudioClip[] nursesounds, instructionsounds, patientsounds;
	public GameObject susan, instructions, leftcontroller, rightcontroller, hintsleft, hintsright, patient;
	public int mugsTrashed, hint, scissorsPlaced, syringesPlaced, tapePlaced, forcepsPlaced, sawsPlaced, scalpolsPlaced;
	public bool toggleHintleft, toggleHintright, toggleHints, bagDone, maskDone;
	private VRTK_ControllerEvents controllerEventsleft, controllerEventsright;
	// Use this for initialization
	void Start() {
		StartCoroutine(BeginProcedure());
		toggleHints = true;
		toggleHintleft = true;
		toggleHintright = true;
		controllerEventsleft = leftcontroller.GetComponent<VRTK_ControllerEvents>();
		controllerEventsright = rightcontroller.GetComponent<VRTK_ControllerEvents>();
	}

	// Update is called once per frame
	void Update() {
		StartCoroutine(DetectHint());
	}
	public IEnumerator BeginProcedure() {
		yield return new WaitForSeconds(6);
		susan.GetComponent<AudioSource>().PlayOneShot(nursesounds[0]);
		yield return new WaitForSeconds(15);
		susan.GetComponent<AudioSource>().PlayOneShot(nursesounds[1]);
		yield return new WaitForSeconds(7);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[0]);
		while (mugsTrashed < 2) {
			yield return null;
		}
		yield return new WaitForSeconds(3);
		susan.GetComponent<AudioSource>().PlayOneShot(nursesounds[2]);
		hint++;
		yield return new WaitForSeconds(7);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[1]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator DetectHint() {
		if (controllerEventsleft.buttonTwoPressed && toggleHints) {
			toggleHints = false;
			hintsleft.transform.GetChild(hint).gameObject.SetActive(toggleHintleft);
			toggleHintleft = !toggleHintleft;
			yield return new WaitForSeconds(0.5f);
			toggleHints = true;
		}
		if (controllerEventsright.buttonTwoPressed && toggleHints) {
			toggleHints = false;
			hintsright.transform.GetChild(hint).gameObject.SetActive(toggleHintright);
			toggleHintright = !toggleHintright;
			yield return new WaitForSeconds(0.5f);
			toggleHints = true;
		}
	}
	public void Event1() {
		StartCoroutine(DOB());
	}
	public void Event2() {
		StartCoroutine(Procedure());
	}
	public void Event3() {
		StartCoroutine(Allergies());
	}
	public void Event4() {
		StartCoroutine(Signature());
	}
	public void Event5() {
		StartCoroutine(Antibiotics());
	}
	public void Event6() {
		StartCoroutine(Scissors());
	}
	public void Event7() {
		StartCoroutine(Syringe());
	}
	public void Event8() {
		StartCoroutine(Tape());
	}
	public void Event9() {
		StartCoroutine(Forceps());
	}
	public void Event10() {
		StartCoroutine(Saw());
	}
	public void Event11() {
		StartCoroutine(Scalpol());
	}
	public void Event12() {
		StartCoroutine(Complete());
	}
	IEnumerator DOB() {
		yield return new WaitForSeconds(1);
		patient.GetComponent<AudioSource>().PlayOneShot(patientsounds[0]);
		yield return new WaitForSeconds(10);
		hint++;
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[2]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Procedure() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		yield return new WaitForSeconds(1);
		patient.GetComponent<AudioSource>().PlayOneShot(patientsounds[1]);
		yield return new WaitForSeconds(6);
		hint++;
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[3]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Allergies() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		yield return new WaitForSeconds(1);
		patient.GetComponent<AudioSource>().PlayOneShot(patientsounds[2]);
		yield return new WaitForSeconds(3);
		hint++;
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[4]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Signature() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		yield return new WaitForSeconds(1);
		patient.GetComponent<AudioSource>().PlayOneShot(patientsounds[3]);
		yield return new WaitForSeconds(6);
		susan.GetComponent<AudioSource>().PlayOneShot(nursesounds[3]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(7);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[5]);
	}
	IEnumerator Antibiotics() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[6]);
		while (!bagDone) {
			yield return null;
		}
		yield return new WaitForSeconds(2);
		susan.GetComponent<AudioSource>().PlayOneShot(nursesounds[4]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		yield return new WaitForSeconds(7);
		hint++;
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[7]);
	}
	IEnumerator Scissors() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(2);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[8]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Syringe() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(2);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[9]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Tape() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(2);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[10]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Forceps() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(2);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[11]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Saw() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(2);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[12]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
		IEnumerator Scalpol() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		hint++;
		yield return new WaitForSeconds(2);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[13]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
	}
	IEnumerator Complete() {
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		yield return new WaitForSeconds(2);
		hint++;
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[14]);
		foreach (Transform child in hintsleft.transform) {
			child.gameObject.SetActive(false);
		}
		foreach (Transform child in hintsright.transform) {
			child.gameObject.SetActive(false);
		}
		while (!maskDone) {
			yield return null;
		}
		yield return new WaitForSeconds(3);
		instructions.GetComponent<AudioSource>().PlayOneShot(instructionsounds[15]);
	}
}
