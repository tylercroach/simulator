﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPatient : MonoBehaviour {

	// Use this for initialization
	public GameObject eventmanager;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name == "Patient" && !eventmanager.GetComponent<Events>().maskDone) {
			transform.position = new Vector3(-3.0093f, 0.8834f, 0.6298f);
			transform.eulerAngles = new Vector3(-90, 0, 180);
			GetComponent<Rigidbody>().isKinematic = true;
			eventmanager.GetComponent<Events>().maskDone = true;

		}

		}
	}
