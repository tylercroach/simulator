﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectSaws : MonoBehaviour {
	public GameObject eventmanager;
	public bool isPlaced;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name == "Tray" && !isPlaced) {
			eventmanager.GetComponent<Events>().sawsPlaced++;
			isPlaced = true;

		}
	}
}
