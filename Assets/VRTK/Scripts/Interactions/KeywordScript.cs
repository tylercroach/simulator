﻿using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KeywordScript : MonoBehaviour
{
		[SerializeField]
		KeywordRecognizer keywordRecognizer;
		public GameObject eventmanager;
		Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

		void Start()
{

    keywords.Add("Please state your name and date of birth", () =>
        {
			eventmanager.GetComponent<Events>().Event1();
        });

    keywords.Add("What procedure are we performing on you and where?", () =>
        {
			eventmanager.GetComponent<Events>().Event2();
        });
	keywords.Add("Do you have any known drug allergies?", () =>
        {
			eventmanager.GetComponent<Events>().Event3();
        });
	keywords.Add("Can you confirm that this is your signature on the consent form please?", () =>
        {
			eventmanager.GetComponent<Events>().Event4();
        });
	keywords.Add("Sir, we will be providing you with antibiotics to prevent skin infection prior to the procedure.", () =>
        {
			eventmanager.GetComponent<Events>().Event5();
        });
	keywords.Add("Two scissors", () =>
        {
			if (eventmanager.GetComponent<Events>().scissorsPlaced == 2) {
				eventmanager.GetComponent<Events>().Event6();
			}
        });
	keywords.Add("One seringe", () =>
        {
			if (eventmanager.GetComponent<Events>().syringesPlaced == 1) {
				eventmanager.GetComponent<Events>().Event7();
			}
        });
	keywords.Add("Two tape", () =>
        {
			if (eventmanager.GetComponent<Events>().tapePlaced == 2) {
				eventmanager.GetComponent<Events>().Event8();
			}
        });
	keywords.Add("One forseps", () =>
        {
			Debug.Log("Forceps");
			if (eventmanager.GetComponent<Events>().forcepsPlaced == 1) {
				eventmanager.GetComponent<Events>().Event9();
			}
        });
	keywords.Add("One saw", () =>
        {
			if (eventmanager.GetComponent<Events>().sawsPlaced == 1) {
				eventmanager.GetComponent<Events>().Event10();
			}
        });
	keywords.Add("One scalpol", () =>
        {
			if (eventmanager.GetComponent<Events>().scalpolsPlaced == 1) {
				eventmanager.GetComponent<Events>().Event11();
			}
        });
	keywords.Add("Patient safety checklist complete. We are now ready to perform the first incision.", () =>
        {
			eventmanager.GetComponent<Events>().Event12();
        });

    // Tell the KeywordRecognizer about our keywords.
    keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

    // Register a callback for the KeywordRecognizer and start recognizing!
    keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
    keywordRecognizer.Start();

}


void movePiece(){
}

private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
{
    System.Action keywordAction;
    if (keywords.TryGetValue (args.text, out keywordAction)) {
        keywordAction.Invoke ();
    }
}
	}